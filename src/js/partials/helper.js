$(document).ready(function () {
    $('.slider').slick({
        prevArrow:'<button type="button" class="slick-prev">Previous</button>',
        nextArrow:'<button type="button" class="slick-next">Next</button>'
    });
});

let age = document.querySelector('.age');
let check = document.querySelector('.check');
let next = document.getElementById('next');
let  okayleft = document.getElementById('okayleft');
let  okayright = document.getElementById('okayright');
let close_modal_window = document.getElementById('close_modal_window');
let close_modal_window1 = document.getElementById('close_modal_window1');
let slider_cont = document.getElementById('slider_cont');
let gendolf = document.getElementById('gendolf');

check.addEventListener('click', function (){
    if(age.value > 18){
        next.classList.add('vis');
        okayleft.classList.add('animL');
        okayright.classList.add('animR');
    }
    else if (age.value >= 18){
        next.classList.add('vis')
        gendolf.classList.remove('modal-vis')
        okayleft.classList.add('animL');
        okayright.classList.add('animR');
    }
    else {
        next.classList.remove('vis')
        gendolf.classList.add('modal-vis')
        okayleft.classList.remove('animL');
        okayright.classList.remove('animR');
    }


    next.addEventListener('click', function (){
        slider_cont.classList.add('modal-vis');
    })

    close_modal_window.addEventListener('click', function (){
        slider_cont.classList.remove('modal-vis');
    })

    close_modal_window1.addEventListener('click', function (){
        gendolf.classList.remove('modal-vis');
    })
})

let start = document.querySelector('.start');
let box_button = document.querySelector('.box_button');
let mult_window = document.querySelector('.mult_window');

start.addEventListener('click', function () {
    box_button.classList.add('hidden')
    mult_window.classList.add('vis')
})








